""""
FIle for encoding and decoding operations"""


import json
from typing import Any, Callable, Generic, NamedTuple, ParamSpec, TypeVar

from ska_tmc_cdm.schemas import CODEC

T = TypeVar("T")
P = ParamSpec("P")

class EncodedObject(Generic[T]):
    """This class takes an command object and can return it in serialised(json/dict) form"""
    def __init__(self, object_to_encode: T):
        self._object_to_encode = object_to_encode

    @property
    def as_json(self) -> str:
        if isinstance(self._object_to_encode, dict):
            return json.dumps(self._object_to_encode)
        return CODEC.dumps(self._object_to_encode)

    @property
    def as_dict(self) -> dict[Any, Any]:
        return json.loads(self.as_json)

    @property
    def as_object(self) -> T:
        return self._object_to_encode



def encoded(func: Callable[P, T]) -> Callable[P, EncodedObject[T]]:
    def inner(*args: P.args, **kwargs: P.kwargs):
        return EncodedObject(func(*args, **kwargs))

    return inner





#assign_request = observation.generate_assign_resources_config().as_object

    # @encoded
    # def generate_assign_resources_config(self, subarray_id: int = 1):
    #     return self._generate_assign_resources_config(subarray_id)
