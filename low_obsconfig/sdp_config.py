from typing import Any, NamedTuple, Tuple, Union, cast
from ska_tmc_cdm.messages.central_node.sdp import SDPConfiguration
from .base import encoded
class SdpConfig():
    def _generate_sdp_assign_resources_config(self):
        return SDPConfiguration(
            eb_id=None,
            interface=None,
            execution_block=None,
            resources=None,
            processing_blocks=None,
        )

    @encoded
    def generate_sdp_assign_resources_config(self):
        return self._generate_sdp_assign_resources_config()