"""
Main entry point class for Jupyter notebook
"""

from ska_tmc_cdm.messages.central_node.assign_resources import AssignResourcesRequest
from .base import encoded
from .mccs import MCCSConfig

class Observation(SdpConfig,MCCSConfig):
    def _generate_assign_resources_config(self, subarray_id: int = 1):
        assign_request = AssignResourcesRequest(
            subarray_id=subarray_id,
            #dish_allocation=self.dish_allocation,
            mccs= self.generate_mccs_assign_resources_config().as_object,
            sdp_config=self.generate_sdp_assign_resources_config().as_object,
            interface=self.assign_resources_schema,
        )
        return assign_request

    @encoded
    def generate_assign_resources_config(self, subarray_id: int = 1):
        return self._generate_assign_resources_config(subarray_id)